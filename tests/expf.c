#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cpuid.h>
#include <immintrin.h>
#include <stdio.h>

#include <stdint.h>
#include "timers.h"
#include "expf.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_expf_vec( float* __restrict__ y, const float* __restrict__ x, int N )
{
//#pragma unroll(4)
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_expf(&x[ii]);
}
//
//
//
void main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	float* __restrict__ x  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ y  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ z  = (float*) malloc(sizeof(float)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 30*((float)rand() / (float)RAND_MAX - .5);
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_expf_vec( y, x, N );
        c1 = rdtscp();
	printf("marla exp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = exp(x[ii]);
        c1 = rdtscp();
        printf("lib    exp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#if 1
	for (int ii = 0; ii < 10; ++ii)
        {
		//if (y[ii] != exp(x[ii]))
			printf("%d: x = %4.15f, exp = %4.15f (%4.15f), error = %4.15f\n", ii, x[ii], y[ii], z[ii], fabs(y[ii] - z[ii]));
        }	
#endif
	//
	free(x);
	free(y);
	//
}

