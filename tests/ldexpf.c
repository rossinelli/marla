#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cpuid.h>
#include <immintrin.h>
#include <stdio.h>

#include <stdint.h>
#include "timers.h"
#include "ldexpf.h"
//
#define NA 1000000
#define NI 10
//
//
//
void marla_ldexpf_vec( float* __restrict__ y, const float* __restrict__ x, int* __restrict__ n, int N )
{
        for (int ii = 0; ii < N; ++ii)
	{
                y[ii] = marla_ldexpf(&x[ii], &n[ii]);
	}
}
//
void main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	float* __restrict__ x  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ y  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ z  = (float*) malloc(sizeof(float)*N);
	int*   __restrict__ n  = (int*)   malloc(sizeof(int)  *N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 2.*(float)rand() / (float)RAND_MAX;
		n[ii] = (int) (rand()%10);
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_ldexpf_vec( y, x, n, N );
        c1 = rdtscp();
	printf("marla ldexp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = ldexpf(x[ii], n[ii]);
        c1 = rdtscp();
	printf("	ldexp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	// 
#if 1
	for (int ii = 0; ii < 10; ++ii)
        {
		//if (y[ii] != exp(x[ii]))
		printf("%d: x = %.15f, pow = %d, val = %.15f (%.15f), x*2^n = %.15f, error = %.15f\n", ii, x[ii], n[ii], y[ii], z[ii], x[ii]*pow(2, n[ii]), fabs(y[ii] - z[ii]));
        }	
#endif
	//
	free(x);
	free(y);
	free(z);
	free(n);
	//
}

