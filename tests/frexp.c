#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cpuid.h>
#include <immintrin.h>
#include <stdio.h>

#include <stdint.h>
#include "timers.h"
#include "frexp.h"
//
#define NA 10000000
#define NI 10
//
//
//
void marla_frexp_vec( double* __restrict__ y, int* __restrict__ z, const double* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_frexp(&z[ii], &x[ii]);
}
//
void main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x  = (double*) malloc(sizeof(double)*N);
	double* y  = (double*) malloc(sizeof(double)*N);
	int*    z  = (int*) malloc(sizeof(int)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = (double)rand() / (double)RAND_MAX;
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_frexp_vec( y, z, x, N );
        c1 = rdtscp();
	printf("marla frexp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
	//
#if 1
	for (int ii = 0; ii < 10; ++ii)
        {
		//if (y[ii] != exp(x[ii]))
			printf("%d: x = %.15f, mantissa = %.15f exp = %2d, val = %.15f, error = %.15f\n", ii, x[ii], y[ii], z[ii], y[ii]*pow(2, z[ii]), fabs(x[ii] - y[ii]*pow(2, z[ii])));
        }	
#endif
	//
	free(x);
	free(y);
	//
}

