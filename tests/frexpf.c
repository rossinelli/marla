#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cpuid.h>
#include <immintrin.h>
#include <stdio.h>

#include <stdint.h>
#include "timers.h"
#include "frexpf.h"
//
#define NA 1000000
#define NI 10
//
//
//
void marla_frexpf_vec( float* __restrict__ y, const float* __restrict__ x, int* __restrict__ n, int N )
{
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_frexpf(&n[ii], &x[ii]);
}
//
void main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	float* __restrict__ x  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ y  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ z  = (float*) malloc(sizeof(float)*N);
	int*   __restrict__ n  = (int*)   malloc(sizeof(int)   *N);
	int*   __restrict__ m  = (int*)   malloc(sizeof(int)   *N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 10*(float)rand() / (float)RAND_MAX - 0.5;
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_frexpf_vec( y, x, n, N );
        c1 = rdtscp();
	printf("marla frexpf : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = frexpf(x[ii], &m[ii]); 	
        c1 = rdtscp();
	printf("lib   frexpf : %ld cycles\n", (c1 - c0)/N); fflush(stdout);

	//
#if 1
	for (int ii = 0; ii < 10; ++ii)
        {
		printf("%d: x = %.15f, mantissa = %.15f (%.15f)exp = %2d (%2d), val = %.15f, error = %.15f\n", ii, x[ii], y[ii], z[ii], n[ii], m[ii], y[ii]*pow(2, n[ii]), fabs(x[ii] - z[ii]*pow(2, m[ii])));
        }	
#endif
	//
	free(x);
	free(y);
	//
}

