#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cpuid.h>
#include <immintrin.h>
#include <stdio.h>

#include <stdint.h>
#include "timers.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
inline
double mydiv(double a, double b)
{
	//return (a/b);
    double r   = 1.f/(float) b;
    r = r*(2. - b*r);
    r = r*(2. - b*r);
    //r = r*(2. - b*r);
    //r = r*(2. - b*r);
    return a*r;
}

void asdasd(double * const __restrict__ x,
double * const __restrict__ y,
double * const __restrict__ z, const int n)
{
#pragma unroll(4)
    for(int i = 0; i < n; ++i)
        z[i] = mydiv(x[i], y[i]);
}
//
//
//
void  main()
{
        const int N     = NA;
        const int NITER = NI;
        //
        double* __restrict__ x  = (double*) malloc(sizeof(double)*N);
        double* __restrict__ y  = (double*) malloc(sizeof(double)*N);
        double* __restrict__ z  = (double*) malloc(sizeof(double)*N);
        //
        srand(time(NULL));
        //
        for (int ii = 0; ii < N; ++ii)
        {
                x[ii] = 100000.*((double)rand() / (double)RAND_MAX);
                y[ii] = 1000.*((double)rand() / (double)RAND_MAX);
        }
        //
        long c0, c1;
        //
        // benchmarking
        //
        c0 = rdtscp();
        asdasd( x, y, z, N );
        c1 = rdtscp();
        printf("exp div %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        //
#if 1
        for (int ii = 0; ii < 10; ++ii)
        {
                //if (y[ii] != exp(x[ii]))
		printf("%d: %.15f/%.15f = %.15f, div = %.15f\n", ii, x[ii],y[ii], z[ii], x[ii]/y[ii], fabs(z[ii] - x[ii]/y[ii]));
        }
#endif
        //
        free(x);
        free(y);
        //
}

