#pragma once
#include "atan.h"
//
void 
atan(double* __restrict__ y, const double* __restrict__ in, int N)
{
	double z;
	short sign, flag;

	for (int i = 0; i < N; ++i)
	{
		myatan_vec(&y[i], &in[i]);
	}
}
//
// atan
//
void myatan2( double* __restrict__ at2, const double* __restrict__ y, const double* __restrict__ x, int N )
{
	for (int ii = 0; ii < N; ++ii)
		at2[ii] = myatan2_vec(&y[ii], &x[ii]);
}
