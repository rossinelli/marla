#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cpuid.h>
#include <immintrin.h>
#include <stdio.h>

#include <stdint.h>
#include "timers.h"
#include "exp.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_exp_vec( double* __restrict__ y, const double* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_exp(&x[ii]);
}
 
//
//
//
void main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x  = (double*) malloc(sizeof(double)*N);
	double* __restrict__ y  = (double*) malloc(sizeof(double)*N);
	double* __restrict__ z  = (double*) malloc(sizeof(double)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 10*((double)rand() / (double)RAND_MAX - .5);
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_exp_vec( y, x, N );
        c1 = rdtscp();
	printf("marla exp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = exp(x[ii]);
        c1 = rdtscp();
        printf("lib    exp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#if 1
	for (int ii = 0; ii < 10; ++ii)
        {
		//if (y[ii] != exp(x[ii]))
			printf("%d: x = %.15f, exp = %.15f (%.15f), error = %.15f\n", ii, x[ii], y[ii], z[ii], fabs(y[ii] - z[ii]));
        }	
#endif
	//
	free(x);
	free(y);
	//
}

