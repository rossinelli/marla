#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cpuid.h>
#include <immintrin.h>
#include <stdio.h>

#include <stdint.h>
#include "timers.h"
#include "log.h"
//
#define NA 1000000
#define NI 10
//
void marla_log_vec( double* __restrict__ y, const double* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
	{
                 y[ii] = marla_log(&x[ii]);
	}
}
//
//
//
void main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x  = (double*) malloc(sizeof(double)*N);
	double* __restrict__ y  = (double*) malloc(sizeof(double)*N);
	double* __restrict__ z  = (double*) malloc(sizeof(double)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = (double)rand() / (double)RAND_MAX;
	}	
	//
	long c0, c1;
	//	
	//
        c0 = rdtscp();
	marla_log_vec( y, x, N );
        c1 = rdtscp();
	printf("marla log : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = log(x[ii]);	
        c1 = rdtscp();
	printf("       log : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
#if 1
	for (int ii = 0; ii < 10; ++ii)
        {
		printf("%d: x = %.15f, log = %.15f (%.15f), error = %.15f\n", ii, x[ii], y[ii], z[ii], fabs(y[ii] - z[ii]));
        }	
#endif
	//
	free(x);
	free(y);
	//
}

