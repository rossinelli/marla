Most Accurate and Rapid Library for Arithmetics: 
- C header-only library
- single and double precision
- implicit vectorization, i.e. no intrinsics (tested with gcc 8.1+ and intel 18+)
- designed to be used with the "-Ofast" compiler option
- no denormal support
- no overflow or underflow


Based on the book:
"Methods and Programs for Mathematical Functions" by Stephen L. Moshier
ISBN 07458-0289-3

authors:
gilles fourestey (gilles.fourestey@epfl.ch)
