#pragma once

static double T3P8 = 2.41421356237309504880;
static double PIO2 = 1.57079632679489661923;
static double PIO4 = 0.785398163397448309616;
static double PI   = 3.14159265358979323846;

static double SNAN  = 0x7F800001;
//static double NAN   = 0xFFFFFFFF;


#define MOREBITS 6.123233995736765886130E-18
#define MAXNUM 1.7976931348623158E308

static
double PA[5] = {
-8.750608600031904122785E-1,
-1.615753718733365076637E1,
-7.500855792314704667340E1,
-1.228866684490136173410E2,
-6.485021904942025371773E1,
};
//
static
double QA[5] = {
/* 1.000000000000000000000E0, */
 2.485846490142306297962E1,
 1.650270098316988542046E2,
 4.328810604912902668951E2,
 4.853903996359136964868E2,
 1.945506571482613964425E2,
};
//
// atan
//
inline
void marla_atan(double* y, const double* in)
{
    /* make argument positive and save the sign */
    int sign = 1;
    double x = *in;
    double z;
    if( x < 0.0 )
	{
	    sign = -1;
	    x = -x;
	}
/* range reduction */
    int flag = 0;
    if( x > T3P8 )
	{
	    *y = PIO2;
	    flag = 1;
	    x = -1.0/x;
	}
    else if( x <= 0.66 ) *y = 0.0;
    else
	{
	    *y = PIO4;
	    flag = 2;
	    x = (x - 1.0)/(x + 1.0);
	}
    z = x * x;
    z = z * polevl( z, PA, 4 ) / p1evl( z, QA, 5 );
    z = x * z + x;
    if( flag == 2 ) z += 0.5 * MOREBITS;
    else if( flag == 1 ) z += MOREBITS;
    *y = *y + z;
    if( sign < 0 ) *y = -(*y);
}
//
// atan2
//
inline
double marla_atan2( const double* py, const double* px )
{
	double z, w;
	double x = *px;
	double y = *py;
	short code;

	code = 0;

	if( x < 0.0 ) code  = 2;
	if( y < 0.0 ) code |= 1;

	if( x == 0.0)
	{
		if( y  < 0.0 ) z = -PIO2;
		else if( y == 0.0 ) z = SNAN;
		else z = PIO2;
	}
	//else
	{
		if( y == 0.0 )
		{
			//z = 0.0;
			if( x < 0.0 ) z = PI; else z = 0.0;
		}
		else
		{
			switch( code )
			{
				default:
				case 0:
				case 1: w = 0.0; break;
				case 2: w =  PI; break;
				case 3: w = -PI; break;
			}
			double ratio = y/x;
			myatan_vec( &z, &ratio);
		}
	}
	//z += w;
	return( z );
}
