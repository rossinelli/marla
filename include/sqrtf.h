#pragma once

#include "frexpf.h"
#include "ldexpf.h"

#define quake3

inline
float marla_sqrtf(float* px)
{
	float x = *px;
        int e;
        short *q;
        float z, w;

        if( x == 0.0f ) return( 0.0f );
        w = x;
#if defined(marla)
        /* Note, frexp and ldexp are used in order to
         * handle denormal numbers properly.
         */
        z = marla_frexpf( &e, &x );
        /* approximate square root of number between 0.5 and 1
         * relative error of approximation = 7.47e-3
         */
        x = (float) 4.173075996388649989089e-1 + (float) 5.9016206709064458299663e-1*z;
        /* adjust for odd powers of 2 */
        if( (e & 1) != 0 ) x *= SQRT2;
        /* re-insert exponent */
        e = e >> 1;
        x = marla_ldexpf( &x, &e );
        /* Note, assume the square root cannot be denormal,
         * so it is safe to use integer exponent operations here.
         */
#elif defined(quake3)
	//if (x == 0.0f) return 0.0f;
	int i;
	float x2, y;
        const float threehalfs = 1.5f;

        x2 = x*0.5f;
        y  = x;
        i  = * ( int * ) &y;                       // evil floating point bit level hacking
        i  = 0x5f3759df - ( i >> 1 );               // what the fuck? 
        y  = * ( float * ) &i;
        y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
      	y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed
      	y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed
        return x*y;	
#else
	x = x/sqrtf(x);
#endif

//
        x = 0.5f*(x + w/x); 
        x = 0.5f*(x + w/x); 
        //x = 0.5f*(x + w/x);  
        return(x);
}
