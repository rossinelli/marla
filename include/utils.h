#pragma once
#include <stdio.h>
static
void printBits(void const* ptr, size_t const size)
{
        unsigned char *b = (unsigned char*) ptr;
        unsigned char byte;
        int i, j;

        size_t pos = 0;

        for (i=size-1; i>=0 ; i--)
        {
                for (j = 7; j >= 0; j--)
                {
                        byte = (b[i] >> j) & 1;
                        printf("%u", byte);
                        pos++;
                        if ((pos == 1) || (pos == 12)) printf(" ");
                }
        }
        puts("");
}

static
void printBits32(void const* const ptr)
{
        unsigned char *b = (unsigned char*) ptr;
        unsigned char byte;
        int i, j;

        size_t pos = 0;

        for (i=4-1; i>=0 ; i--)
        {
                for (j = 7; j >= 0; j--)
                {
                        byte = (b[i] >> j) & 1;
                        printf("%u", byte);
                        pos++;
                        if ((pos == 1) || (pos == 9)) printf(" ");
                }
        }
        puts("");
}

