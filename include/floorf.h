#pragma once

#define EXPMSK 0x800f
#define MEXP 0x7ff
#define FNBITS 53

static unsigned short bmask[] = {
	0xffff,
	0xfffe,
	0xfffc,
	0xfff8,
	0xfff0,
	0xffe0,
	0xffc0,
	0xff80,
	0xff00,
	0xfe00,
	0xfc00,
	0xf800,
	0xf000,
	0xe000,
	0xc000,
	0x8000,
	0x0000,
};

float floor_old(float x)
{
	union
	{
		float y;
		unsigned short sh[4];
	} u;
	unsigned short *p;
	int e;


	u.y = x;
	/* find the exponent (power of 2) */
	p = (unsigned short *)&u.sh[3];
	e = (( *p >> 4) & 0x7ff) - 0x3ff;
	p -= 3;

	if( e < 0 )
	{
		if( u.y < 0.0 )
			return( -1.0 );
		else
			return( 0.0 );
	}

	e = (FNBITS -1) - e;
	/* clean out 16 bits at a time */
	while( e >= 16 )
	{
		*p++ =  0;
		e   -= 16;
	}

	/* clear the remaining bits */
	if (e > 0) * p&= bmask[e];

	if( (x < 0) && (u.y != x) ) u.y -= 1.0;

	return(u.y);
}

float marla_floorf(float* x)
{
	return ((*x < 0.0) ? (float) ((long long) x) : (float) ((long long) x - 1.));
	
}
