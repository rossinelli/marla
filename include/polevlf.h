inline
float marla_polevlf( float x, float coef[], int N )
{
    float ans;
    int i;
    float *p;

    p = coef;
    ans = *p++;
    i = N;

    do
    {
        ans = ans * x  +  *p++;
    }
    while( --i );

    return( ans );
}

/* p1evl() */
/* N
 * Evaluate polynomial when coefficient of x  is 1.0.
 * Otherwise same as polevl.
 */

inline
float marla_p1evlf( float x, float coef[], int N )
{
    float ans;
    float *p;
    int i;

    p = coef;
    ans = x + *p++;
    i = N - 1;

    do
    {
        ans = ans * x  + *p++;
    }
    while( --i );

    return( ans );
}

