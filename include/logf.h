#pragma once
#include <stdint.h>
//#include "timers.h"
#include "utils.h"
#include "frexpf.h"
#include "polevlf.h"
//
static char fname[] = {"log"};

/* Coefficients for log(1+x) = x - x**2/2 + x**3 P(x)/Q(x)
 * 1/sqrt(2) <= x < sqrt(2)
 */
static float P[] = {
 1.01875663804580931796E-4,
 4.97494994976747001425E-1,
 4.70579119878881725854E0,
 1.44989225341610930846E1,
 1.79368678507819816313E1,
 7.70838733755885391666E0,
};
static float Q[] = {
/* 1.00000000000000000000E0, */
 1.12873587189167450590E1,
 4.52279145837532221105E1,
 8.29875266912776603211E1,
 7.11544750618563894466E1,
 2.31251620126765340583E1,
};

/* Coefficients for log(x) = z + z**3 P(z)/Q(z),
 * where z = 2(x-1)/(x+1)
 * 1/sqrt(2) <= x < sqrt(2)
 */

static float R[3] = {
-7.89580278884799154124E-1,
 1.63866645699558079767E1,
-6.41409952958715622951E1,
};
static float S[3] = {
/* 1.00000000000000000000E0,*/
-3.56722798256324312549E1,
 3.12093766372244180303E2,
-7.69691943550460008604E2,
};

extern float marla_ldexpf ( float, int );
//
#define SQRTH 0.70710678118654752440f
//
inline
float marla_logf(float* px)
{
	//printf("%f\n", *px);
	int ie;
        float x = *px, y, z, w, e;

        /* separate mantissa from exponent */

        /* Note, frexp is used so that denormal numbers
         * will be handled properly.
         */

        /* Equivalent C language standard library function: */
        x = marla_frexpf( &ie, &x );
        e = (float) ie;
        /* logarithm using log(x) = z + z**3 P(z)/Q(z),
         * where z = 2(x-1)/x+1)
         */
        if(abs (ie > 2))
        {
                if( x < SQRTH )
                { /* 2( 2x-1 )/( 2x+1 ) */
                        e -= 1;
                        z = x - 0.5f;
                        y = 0.5f*z + 0.5f;
                }
                else
                { /*  2 (x-1)/(x+1)   */
                        z = x - 0.5f;
                        z -= 0.5f;
                        y = 0.5f*x  + 0.5f;
                }
                x = z / y;

                /* rational form */
                z = x*x;
                z = x*(z*marla_polevlf(z, R, 2)/marla_p1evlf(z, S, 3));
                z = z - e*2.121944400546905827679e-4f;
                z = z + x;
                z = z + e*0.693359375f;
        }
        else
        {

                /* logarithm using log(1+x) = x - .5x**2 + x**3 P(x)/Q(x) */
                if( x < SQRTH )
                {
                        e -= 1;
                        x = 2.f*x - 1.0f; /*  2x - 1  */
                }
                else x = x - 1.0f;
                /* rational form */
                z = x*x;
                y = x*x*x*marla_polevlf(x, P, 5)/marla_p1evlf(x, Q, 5);
                y = y - e*2.121944400546905827679e-4f;
                y = y - 0.5f*x*x;   /*  y - 0.5 * z  */
                z = x + y + (float) e*0.693359375f;
        }
	return z;
}

