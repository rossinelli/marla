#pragma once


#include <stdio.h>

#include "const.h"
#include "ldexp.h"
#include "polevl.h"
#include "div.h"


/*	Exponential function	*/

static double PP[] = {
	1.26177193074810590878E-4,
	3.02994407707441961300E-2,
	9.99999999999999999910E-1,
};
static double QQ[] = {
	3.00198505138664455042E-6,
	2.52448340349684104192E-3,
	2.27265548208155028766E-1,
	2.00000000000000000009E0,
};

static double C1 = 6.93145751953125E-1;
static double C2 = 1.42860682030941723212E-6;

extern double floor ( double );
extern double ldexp ( double, int );
extern double LOGE2, LOG2E, MAXLOG, MINLOG, MAXNUM;

inline
double marla_exp(double* in)
{
	double px, xx, x = *in;
	int n;

	/* Express e**x = e**g 2**n
	 *   = e**g e**( n loge(2) )
	 *   = e**( g + n loge(2) )
	 */
	px = floor (LOG2E * x + 0.5);	/* floor() truncates toward -infinity. */
	//printf("--> %f %f\n", LOG2E * x + 0.5, px); fflush(stdout);
	n = px;
	//
	x -= px * C1;
	x -= px * C2;
	/* 
	 * rational approximation for exponential
	 * of the fractional part:
	 * e**x = 1 + 2x PP(x**2)/( QQ(x**2) - PP(x**2) )
	 */
	xx = x*x;
	px = x*marla_polevl (xx, PP, 2);
	//x  = px/(polevl (xx, QQ, 3) - px);
	x  = marla_div(px, (marla_polevl (xx, QQ, 3) - px));
	x  = 1.0 + 2.0*x;

	/* multiply by power of 2 */
	//x = ldexp (x, n);
	double p2 = (double) (1 << abs(n));
	if (n < 0) p2 = marla_div(1., p2);
	x = x*p2;
	//
	return (x);
}
