    #include <stdio.h>
    #include <stdlib.h>
static
__inline__ uint64_t rdtsc(void) {
        uint32_t l, h;
        __asm__ __volatile__ ("xorl %%eax,%%eax\ncpuid"
                              ::: "%rax", "%rbx", "%rcx", "%rdx");
        __asm__ __volatile__ ("rdtsc" : "=a" (l), "=d" (h));
        return (uint64_t)h << 32 | l;
}

static
__inline__
unsigned long long rdtscp()
{
        unsigned int cycles_high, cycles_low;
        __asm__ volatile (
                        "RDTSCP\n\t"/*read the clock*/
                        "mov %%edx, %0\n\t"
                        "mov %%eax, %1\n\t"
                        "CPUID\n\t": "=r" (cycles_high), "=r"
                        (cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
        return ((unsigned long long)cycles_low + ((unsigned long long)cycles_high << 32));
}

