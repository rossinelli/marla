#pragma once

double MACHEP   =  1.11022302462515654042E-16;   /* 2**-53 */
double UFLOWTHRESH =  2.22507385850720138309E-308; /* 2**-1022 */
double MAXLOG   =  7.08396418532264106224E2;     /* log 2**1022 */
double MINLOG   = -7.08396418532264106224E2;     /* log 2**-1022 */
double MAXNUM   =  1.79769313486231570815E308;    /* 2**1024*(1-MACHEP) */
double PI       =  3.14159265358979323846;       /* pi */
double PIO2     =  1.57079632679489661923;       /* pi/2 */
double PIO4     =  7.85398163397448309616E-1;    /* pi/4 */
double SQRT2    =  1.41421356237309504880;       /* sqrt(2) */
double SQRTH    =  7.07106781186547524401E-1;    /* sqrt(2)/2 */
double LOG2E    =  1.4426950408889634073599;     /* 1/log(2) */
double SQ2OPI   =  7.9788456080286535587989E-1;  /* sqrt( 2/pi ) */
double LOGE2    =  6.93147180559945309417E-1;    /* log(2) */
double LOGSQ2   =  3.46573590279972654709E-1;    /* log(2)/2 */
double THPIO4   =  2.35619449019234492885;       /* 3*pi/4 */
double TWOOPI   =  6.36619772367581343075535E-1; /* 2/pi */
//double INFINITY =  1.79769313486231570815E308;    /* 2**1024*(1-MACHEP) */
//double NAN      = 0.0;
double NEGZERO  = 0.0;
