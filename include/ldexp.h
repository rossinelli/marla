#pragma once

#include <stdio.h>
#include "const.h"
#include "utils.h"

#if 0
inline
double marla_ldexp(double* px, int* ppw2)
{
	double x   = *px;
	int    pw2 = *ppw2;
	union
	{
		double y;
		unsigned short sh[4];
	} u;
	short *q;
	int e;


	u.y = x;
	q = (short *)&u.sh[3];
	int ee =  (*q & 0x7ff0);
	ee =  (*q & 0x7ff0) >> 4;
	while( (e = (*q & 0x7ff0) >> 4) == 0 )
	{
		if( u.y == 0.0 )
		{
			return( 0.0 );
		}
		/* Input is denormal. */
		if( pw2 > 0 )
		{
			u.y *= 2.0;
			pw2 -= 1;
		}
		if( pw2 < 0 )
		{
			if( pw2 < -53 )
				return(0.0);
			u.y /= 2.0;
			pw2 += 1;
		}
		if( pw2 == 0 )
			return(u.y);
	}
	e += pw2;

	/* Handle overflow */
	//if( e >= MEXP ) return( 2.0*MAXNUM );

	/* Handle denormalized results */
	if (e < 1) return (0.0);
	else
	{
		*q &= 0x800f;
		*q |= (e & 0x7ff) << 4;
	}
	return u.y;
}
#endif
//
inline
double marla_ldexp( double* x, int* pw2 )
{
	//
	uint64_t z = *(uint64_t *) x;
	uint32_t e; 
	//	
        e = (z >> 52) & 0x7ff;
	e += *pw2;
	//
	//z = (uint64_t) (e & 0x7ff) << 52 | z & 0x800fffffffffffff; 
	z = (uint64_t) e << 52 | z & 0x800fffffffffffff; 
	return *(double*) &z;
}

